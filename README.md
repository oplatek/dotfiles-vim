Installation
============

```sh
git clone this_repo_url  /path/to/vim/portable/direcotry
# For example
git clone git@bitbucket.org:oplatek/dotfiles-vim.git $HOME/.vim
```

```sh
alias vim='vim -u /path/to/vim/portable/direcotry/vimrc'
```
*UPDATE*
On Vim 7.4 you just clone this repository to `$HOME/.vim`.

*Pull requests welcomed!*
